# Хөгжүүлэлтийн орчинг вагрант ашиглан бэлтгэх.
### Суулгах шаардлагатай программууд
 - [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
 - [Vagrant](https://www.vagrantup.com/downloads.html)
 - [Git](https://git-scm.com/downloads)
 - [Powershell 3](https://www.microsoft.com/en-us/download/details.aspx?id=34595) эсвэл [cmder](http://cmder.net/) Windows 7 хэрэглэдэг бол ямар нэг unicode дэмждэг командын цонх хэрэгтэй.

### Ажиллуулах зааварчилгаа
Сонгосон хавтаснаас коммандын цонх нээгээд доорх коммандуудыг ажиллуулах.
```
git clone https://gitlab.com/inpeace/vagrant
cd vagrant
vagrant up
```
### Жишээ болгон бэлдсэн орчны дэлгэрэнгүй
 - Үйлдлийн систем - Ubuntu 16.04 LTS
 - Вэб сервер - Apache 2
 - Өгөгдлийн сан - Mysql
 - Вэб хөгжүүлэх програмчлалын хэлүүд - PHP, NodeJS, Ruby on Rails.

### Хандалтууд
 - Apache вэб серверийн хандалт    http://localhost:3001
 - PhpMyAdmin-ний хандалт          http://localhost:3001/phpmyadmin | username, password - root, peace 
 - Виртуал машины 3000 порт дээр ачаалж буй вэб рүү хандах. http://localhost:3000
 - Виртуал машины коммандын цонх руу шилжих бол: 

``` bash
vagrant ssh
```

### Файлын бүтэц
/ - .gitignore  - git аль файлуудын өөрчлөлтийг санах талаарх тохиргоо

/ - README.md - Тайлбар бүхий файл

/ - Vagrantfile - Вагрантын үндсэн тохиргооны файл. Виртуал машинтай холбоотой тохиргоонууд

/ - provision.sh - Үүсгэх хөгжүүлэлтийн орчны тохиргоо буюу зааварчилгаа

### Тэмдэглэл
vagrant up коммандыг эхний удаад өгсөнөөр виртуал машин шинээр үүсгэж орчин бүрдүүлэлтийг хийнэ.
Комманд ажиллах явцдаа виртуал орчний түүлүүдийн хувилбарын мэдээллийг provision.log файл руу бичнэ.
Коммандуудын тайлбарыг shell файлуудаас харах боломжтой.
