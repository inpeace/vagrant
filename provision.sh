# орчин бэлдсэн лог бичих файлын нэр
log=/vagrant/provision.log

# лог бичих файлыг үүсгэ
touch $log && echo "Бэлдсэн орчны дээрх хэл түүлүүдийн хувилбарууд" > $log

# yarn - г apt - ийн жагсаалтад нэм
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
# apt - г шинэчил
sudo apt update

# yarn, npm, node суулга
sudo apt install yarn npm -y
echo "Yarn" >> $log && yarn -v >> $log
echo "NPM" >> $log && npm -v >> $log
sudo npm cache clean -f && sudo npm install -g n && sudo n stable
echo "NodeJS" >> $log && node -v >> $log

# apache2 суулга
sudo apt install apache2 -y
apache2 -v >> $log

# mysql суулга, үндсэн нэвтрэх нэр root, нууц үг peace
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password peace'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password peace'
sudo apt install libmysqlclient-dev mysql-server -y
mysql -u root -ppeace -e "CREATE DATABASE example_db CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql --version >> $log

# php суулга
sudo apt install php libapache2-mod-php php-mcrypt php-mysql -y
sudo service apache2 restart
php -v >> $log

# phpmyadmin суулга, нэвтрэх нэр root, нууц үг peace
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password peace"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password peace" 
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password peace"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt install phpmyadmin php-mbstring php-gettext -y
sudo phpenmod mcrypt
sudo phpenmod mbstring
sudo a2enmod rewrite
sudo service apache2 restart

# ruby 2.4.2 - г rvm ашиглан суулга
RUBY_VERSION="2.4.2"
sudo apt update
if ! type rvm >/dev/null 2>&1; then
  curl -sSL https://rvm.io/mpapis.asc | gpg --import -
  curl -L https://get.rvm.io | bash -s stable
  source /etc/profile.d/rvm.sh
fi
if ! rvm list rubies ruby | grep ruby-${RUBY_VERSION}; then
  rvm install ${RUBY_VERSION}
fi
rvm --default use ${RUBY_VERSION}
rvm all do gem install middleman
rvm_silence_path_mismatch_check_flag=1 >> ~/.rvmrc
sudo chown -R vagrant:vagrant /usr/local/rvm/
rvm -v >> $logx

# rails 5.1.4 - г суулга
gem install rails -v 5.1.4
rails -v >> $log

# ssh key. git ашигладаг бол машинд шинэ ssh key-г peace@busy.mn имэйлд харгалзуулах үүсгэж авах
ssh-keygen -t rsa -C "peace@busy.mn" -b 4096 -f /home/vagrant/.ssh/id_rsa -q -P ""
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa.pub
echo "\r\n" && echo "Git server лүү хандахад зориулсан нууц үггүй түлхүүрийг доор хэвлэлээ."
cat /home/vagrant/.ssh/id_rsa.pub
echo "\r\n" && echo "Орчин бэлэн болсон vagrant ssh коммандаар нэвтэрч орно уу."

